# Tarot Server
API realisé pour servir un site de cartes tarot. Cette API est devisé en trois parties:
- Cartes tarot
- Blog 
- Utilisateur

## Cartes Tarot
- lecture, creation, edition et elimination des cartes
- filtrage des cartes par nom de la carte
- generation aleatoire d'une liste de 10 cartes tarot
- generation aleatoire de numero (dé)

## Blog
- lecture, creation, edition et elimination de publications

## Utilisateur
- Creation d'utilisateur
- Login

## Dependences
- bcrypt
- cors
- express
- mongodb
- mongoose

## Installation
```
npm install
```
```
npm start
```

## License

License ISC

Crée par Joana Martins

https://gitlab.com/ajoanaverde

https://github.com/ajoanaverde