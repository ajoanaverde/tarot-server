//
// IMPORTS (libraries)
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const http = require('http');
//
// IMPORTS
const routes = require('./routes');
//
// app initialisation
const app = express()
//
// connection db
const uri = "mongodb+srv://Joana:9V0FxNb7lDRTR9sz@clusterjo-veg5f.mongodb.net/ProjetTarot?retryWrites=true&w=majority";
//
mongoose.connect(uri, {
    useNewUrlParser: true
}).then(() => {
    console.log("Connection mongodb ok")
}).catch(err => {
    console.log(err)
})
//
//compabilité
app.use(cors());
//body parsing middleware 
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
//
// ROUTES
app.use('/', routes);
//
// EXPORTS
module.exports = app;
//
//
//
//