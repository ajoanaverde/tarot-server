//
// IMPORTS (libraries)
const http = require('http');
//
// IMPORTS
const app = require('./app');
//
// port verification
const portOk = val => {
    const port = parseInt(val, 10);
    if (isNaN(port)) {
        return val;
    }
    if (port >= 0) {
        return port;
    }
    return false;
}
//
// error handler (look into it)
const errorHandler = error => {
    if (error.syscall !== 'listen') {
        const address = server.address();
        const bind = typeof address === 'string' ? 'pipe ' + address : 'port: ' + port;
        switch (error.code) {
            case 'EACCESS':
                console.error(bind + 'Permission denied');
                // process.exit(1);
                break;
            case 'EADDRINUSE':
                console.error(bind + 'Adress already in use');
                // process.exit(1);
                break;
            default:
                throw error;
        }
    }
}
//
// creating server
const server = http.createServer(app);
// checking for errors with my error handler
server.on('error', errorHandler);
// test and then comment out (what ?)
server.on('listening', () => {
    const address = server.address();
    console.log('Listen on port ' + port);
});
//
// port 
const port = portOk(process.env.PORT || 3000);
app.set('port', port);
//
//server listen
server.listen(port);
//
//