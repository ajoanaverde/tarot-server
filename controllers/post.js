//
// IMPORTS (libraries)
const mongoose = require('mongoose');
//
// IMPORTS
const Post = require('../models/Post');
//
////////////
// CREATE//
//////////
//
exports.createPost = (req, res) => {
    const post = req.body;
    const postOk = new Post({
        ...post
    })
    console.log(postOk);
    postOk.save().then(result => {
        res.status(201).json({
            message: "POst crée"
        })
    }).catch(err => {
        res.status(400).json({ message: "Une erreur est survenue" })
    })
}
//
/////////////
//  GET   //
//  ALL  //
//////////
//
exports.getPosts = (req, res) => {
    Post.find().then(posts => {
        res.status(200).json(posts);
    }).catch(err => {
        res.status(400).json(err);
    })
}
//
//////////////////
//  GET BY ID  //
////////////////
//
exports.getPostById = (req, res) => {
    Post.findById(req.params.id).then(post => {
        res.status(200).json(post);
    }).catch(err => {
        res.status(400).json(err);
    })
}
// ADD A 404
//
///////////////
//  UPDATE  //
/////////////
// look into it
exports.updatePost = (req, res) => {
    if (!req.body) {
        return res.status(400).send({
            message: "Data to update can not be empty!"
        });
    }
    console.log(req.body);
    // const postUpdated = new Post({
    //     ...req.body
    // })
    // mongoose.set('useFindAndModify', false);
    Post.findByIdAndUpdate(req.params.id, req.body, { useFindAndModify: false }).then(result => {
        //console.log(result);
        res.status(201).json({ message: "Le post a ete mis a jour" })
    }).catch(err => {
        console.log(err);
        res.status(400).json(err)

    })
}
//
///////////////
//  DELETE  //
/////////////
//
// will maybe need a delete many or delete all
exports.deletePost = (req, res) => {
    mongoose.set('useFindAndModify', false);
    Post.findByIdAndDelete(req.params.id).then(() => {
        res.status(201).json({ message: "Le post a ete suprime" })
    }).catch(err => {
        res.status(400).json(err)
    })
}
//
////////////////
//  INSERT   //
//   MANY   //
/////////////
//
exports.insertManyPosts = (req, res) => {
    Post.insertMany([
        //inserer données ici
    ]
    ).then(result => {
        res.status(201).json({
            message: 'Ok !'
        })
    }).catch(err => {
        res.status(400).json(err)
    });
}