function spin(max) {
    return Math.floor((Math.random() * max)+1);
}
//
exports.classicDice = (req, res) => {
    const result = spin(6);
    res.json(result);
}
