//
// IMPORTS (libraries)
const mongoose = require('mongoose');
//
// IMPORTS
const CardGroup = require('../models/CardGroup');
//
////////////
// CREATE//
//////////
//
exports.createCardGroup = (req, res) => {
    const cardGroup = req.body;
    const cardGroupOk = new CardGroup({
        ...cardGroup
    })
    console.log(cardGroupOk);
    cardGroupOk.save().then(result => {
        res.status(201).json({
            message: "Groupe de cartes crée"
        })
    }).catch(err => {
    res.status(400).json({message: "Une erreur est survenue"})
})

}
//
/////////////
//  GET   //
//  ALL  //
//////////
//
exports.getCardGroups = (req, res) => {
    CardGroup.find().then(cardGroups => {
        res.status(200).json(cardGroups);
    }).catch(err => {
        res.status(400).json(err);
    })
}
//
//
//////////////////
//  GET BY ID  //
////////////////
//
exports.getCardGroupById = (req, res) => {
    CardGroup.findById(req.params.id).then(cardGroup => {
        res.status(200).json(cardGroup);
    }).catch(err => {
        res.status(400).json(err);
    })
}
// ADD A 404
//
//
///////////////
//  UPDATE  //
/////////////
//
exports.updateCardGroup = (req, res) => {
    const cardGroupUpdated = new CardGroup({
        ...req.body
    })
    mongoose.set('useFindAndModify', false);
    CardGroup.findByIdAndUpdate(req.params.id, cardGroupUpdated).then(result => {
        res.status(201).json({message: "Le groupe de cartes a ete mis a jour"})
    }).catch(err => {
        res.status(400).json(err)
    })
}
//
//
//
///////////////
//  DELETE  //
/////////////
//
exports.deleteCardGroup = (req, res) => {
    mongoose.set('useFindAndModify', false);
    CardGroup.findByIdAndDelete(req.params.id).then(() => {
        res.status(201).json({message: "Le groupe de cartes a ete suprime"})
    }).catch(err => {
        res.status(400).json(err)
    })
}