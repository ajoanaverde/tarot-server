//
// IMPORTS (libraries)
const mongoose = require('mongoose');
//
// IMPORTS (models)
const Card = require('../models/Card');
//
//////////////
// CREATE ///
////////////
//
exports.createCard = (req, res) => {
    const card = req.body;
    const cardOk = new Card({
        ...card
    })
    console.log(cardOk);
    cardOk.save().then(result => {
        res.status(201).json({
            message: "CArte crée"
        })
    }).catch(err => {
        res.status(400).json({ message: "Une erreur est survenue" })
    })
}
//
/////////////////
//  GET ALL  ///
//   CARDS  ///
//////////////
//
exports.getCards = (req, res) => {
    const name = req.query.name;
    // filter for search
    let condition = name ? { name: { $regex: new RegExp(name), $options: "i" } } : {};
    // i = Case-insensitive search.	
    //
    Card.find(condition).then(cards => {
        res.status(200).json(cards);
    }).catch(err => {
        res.status(400).json(err);
    })
}
//
///////////////
/// RANDOM ///
/////////////
//
exports.getRandomCards = (req, res) => {
    Card.find().then(cards => {
        let reading = [];
        let i = 0;
        while (i < 10) {
            reading.push(cards.splice(
                Math.floor((Math.random() * (cards.length - 1)) + 1), 1
            )[0]);
            i++;
        }
        res.send(reading);
    }).catch(err => {
        res.status(400).json(err);
    })
}
//
///////////////////
//    GET ALL  ///
//  MEANINGS  ///
////////////////
// maybe not nedded
exports.getCardMeanings = (req, res) => {
    Card.find().select('meaning').then(meanings => {
        res.status(200).json(meanings);
    }).catch(err => {
        res.status(400).json(err);
    })
}
//
///////////////////
//    GET ALL  ///
//    NAMES   ///
////////////////
// maybe not nedded
exports.getCardNames = (req, res) => {
    Card.find().select('name').then(names => {
        res.status(200).json(names);
    }).catch(err => {
        res.status(400).json(err);
    })
}
//
///////////////////
//  GET BY ID  ///
/////////////////
//
// ADD A 404
//
exports.getCardById = (req, res) => {
    Card.findById(req.params.id).then(card => {
        res.status(200).json(card);
    }).catch(err => {
        res.status(400).json(err);
    })
}
//
////////////////////
//  GET MEANING ///
//     BY ID   ///
/////////////////
// maybe not nedded
// ADD A 404
//
exports.getMeaningById = (req, res) => {
    Card.findById(req.params.id).select('meaning').then(meaning => {
        res.status(200).json(meaning);
    }).catch(err => {
        res.status(400).json(err);
    })
}
//
////////////////
//  UPDATE  ///
//////////////
//
exports.updateCard = (req, res) => {
    /*const cardUpdated = new Card({
        ...req.body
    })
    mongoose.set('useFindAndModify', false);
    Card.findByIdAndUpdate(req.params.id, cardUpdated).then(result => {
        res.status(201).json({ message: "La carte a ete mis a jour" })
    }).catch(err => {
        res.status(401).json(err)
    })*/

    Card.findById(req.params.id).then(card => {
        card.update(req.body).then(result => {
            res.status(200).send({message: ok})
        }).catch(err => res.send(err))
    })
    // merci enzo
}
//
////////////////
//  DELETE  ///
//////////////
// need a delete many or delete all
exports.deleteCard = (req, res) => {
    mongoose.set('useFindAndModify', false);
    Card.findByIdAndDelete(req.params.id).then(() => {
        res.status(201).json({ message: "La carte a ete suprime" })
    }).catch(err => {
        res.status(400).json(err)
    })
}
//
/////////////////
//  INSERT   ///
//   MANY   ///
//////////////
//
exports.insertManyCards = (req, res) => {
    Card.insertMany([
        //inserer données ici
        
    ]
    ).then(result => {
        res.status(201).json({
            message: 'Ok !'
        })
    }).catch(err => {
        res.status(400).json(err)
    });
}

// module.exports = allCards = this.getCardNames();