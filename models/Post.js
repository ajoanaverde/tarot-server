//
// IMPORTS (libraries)
const mongoose = require('mongoose');
// call the schema object
const Schema = mongoose.Schema;
//
// defining the schema
const postSchema = new Schema({
    title: String,
    body: String
    // try this, and if it works, delete all the previous posts
    // {
    //     title: String,
    //     description: String,
    //   },
    //   { timestamps: true }
});
//
// gets rid ov "__v" and can change the "__id" for "id" (for exemple)
postSchema.method("toJSON", function () {
    const { __v, _id, ...object } = this.toObject();
    object._id = _id;
    return object;
})
//
// EXPORTS
module.exports = mongoose.model('Post', postSchema);