//
// IMPORTS (libraries)
const mongoose = require('mongoose');
// call the schema object
const Schema = mongoose.Schema;
//
// defining the schema
const cardSchema = new Schema({
    number: String,
    name: String,
    image: String,
    meaning: {
        general: String,
        self: String,
        challenge: String,
        conscious: String,
        unconscious: String,
        past: String,
        future: String,
        inner: String,
        outer: String,
        fear: String,
        outcome: String
    }
})
//
// gets rid ov "__v" and can change the "__id" for "id" (for exemple)
cardSchema.method("toJSON", function () {
    const { __v, _id, ...object } = this.toObject();
    object._id = _id;
    return object;
})
//
//
// EXPORTS
module.exports = mongoose.model('Card', cardSchema);