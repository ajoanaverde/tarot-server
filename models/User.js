//
// IMPORTS (libraries)
//const mongoose = require('mongoose');
//const bcrypt = require('bcrypt');
// call the schema object
//const Schema = mongoose.Schema;
//
// defining the schema
//const userSchema = new Schema({
//    username: { type: String, require: true},
//    password: String
//});
// hash the password
//userSchema.methods.generateHash = function (password) {
//    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
//};
// checking if password is valid
//userSchema.methods.validPassword = function (password) {
//    return bcrypt.compareSync(password, this.password);
//};
//
// EXPORTS
//module.exports = mongoose.model('User', userSchema);