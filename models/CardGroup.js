//
// IMPORTS (libraries)
const mongoose = require('mongoose');
// call the schema object
const Schema = mongoose.Schema;
//
// defining the schema
const cardGroupSchema = new Schema({
    group: {type: Array, minItems: 2, maxItems: 10},
    meaning: [{
        description: String,
        fortune: String,
    }]
})
//
// EXPORTS
module.exports = mongoose.model('CardGroup', cardGroupSchema);