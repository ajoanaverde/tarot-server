//
// IMPORTS
const express = require('express')
const router = express.Router()
//
// IMPORTS (controllers)
const cardsController = require('./controllers/card');
const postsController = require('./controllers/post');
const cardGroupController = require('./controllers/cardGroup');
const userController = require('./controllers/user');
const dice = require('./controllers/de');
//
// ROUTES
//
//Basic routing example:
//app.METHOD(PATH, HANDLER)
//
//delete
//
router.delete('/cards/:id', cardsController.deleteCard);
router.delete('/posts/:id', postsController.deletePost);
router.delete('/group/:id', cardGroupController.deleteCardGroup);
//
//update
//
router.put('/cards/:id', cardsController.updateCard);
router.put('/posts/:id', postsController.updatePost);
router.put('/group/:id', cardGroupController.updateCardGroup);
//
//get by id
//
router.get('/cards/:id', cardsController.getCardById);
router.get('/posts/:id', postsController.getPostById);
router.get('/group/:id', cardGroupController.getCardGroupById);
router.get('/card-meanings/:id', cardsController.getMeaningById)
//
//create
//
router.post('/cards', cardsController.createCard);
router.post('/posts', postsController.createPost);
router.post('/group', cardGroupController.createCardGroup);
//
//create a user
//
router.post('/newuser', userController.createUser);
//
//login
//
router.post('/login', userController.loginUser);
//
//get all
//
router.get('/cards', cardsController.getCards);
router.get('/card-meanings', cardsController.getCardMeanings);
router.get('/card-names', cardsController.getCardNames);
router.get('/posts', postsController.getPosts);
router.get('/group', cardGroupController.getCardGroups);
//
//insert many
//
router.get('/insert-many-cards', cardsController.insertManyCards);
router.get('/insert-many-posts', postsController.insertManyPosts);
//
// tirage cartes
//
router.get('/tirage', cardsController.getRandomCards);
//
// de magique
//
router.get('/de', dice.classicDice);

module.exports = router;